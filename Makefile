TARGET=main
ASM=nasm
ASMFLAGS=-f elf64
LD=ld
SOURCE=$(wildcard *.asm)
OBJECT=$(SOURCE:.asm=.o)

.PHONY: all clean

all: $(TARGET)

%.o: %.asm
	$(ASM) $(ASMFLAGS) $< -o $@

$(TARGET): $(OBJECT)
	$(LD) $^ -o $@
	
clean:
	rm *.o
