%include "lib.inc"
section .text
global find_word
; в rdi строка, в rsi ссылка на 1-й элемент
find_word:
    push rsi
    push rdi
    add rsi, 8
    call string_equals
    pop rdi
    pop rsi
    cmp rax, 1
    je .found
    mov rsi, [rsi]
    cmp rsi, 0
    je .not_found
    jmp find_word
    .found:
        mov rax, rsi
        ret
    .not_found:
        mov rax, 0
        ret
