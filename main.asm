section .rodata
%include "words.inc"
err: 
db "Нет такой строки",`\n` , 0
section .bss
%define SIZE 256
bufer: resb SIZE

section .text
%include "lib.inc"
%include "dict.inc"
global _start
_start:
    mov rdi, bufer
    mov rsi, SIZE
    call read_word
    mov rdi, rax
    mov rsi, list_start
    call find_word
    cmp rax, 0
    je .gg
    add rax, 8
    mov rdi, rax
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    call print_newline
    mov rdi, 0
    call exit

    .gg:
        mov rdi, err
        call print_err
        mov rdi, 1
        call exit
